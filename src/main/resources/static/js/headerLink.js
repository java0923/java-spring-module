document.addEventListener("DOMContentLoaded", function() {
  var currentPath = window.location.pathname;
  var menuLinks = document.querySelectorAll('.navbar li a');

  menuLinks.forEach(function(link) {
    var linkPath = new URL(link.href).pathname;
    if (currentPath.includes(linkPath)) {
      link.parentNode.classList.add('active');
    }
  });
});