package com.sber.spring.springproject.dbexample.dao;

import java.util.List;

public interface IDaoLayer<T, ID> {
    T getOneObjectById(ID id);

    List<T> getAllObjects();

    T create(T newObject);

    T update(T updatedObject, ID id);

    void deleteObjectById(ID id);
}
