package com.sber.spring.springproject.dbexample.model;

import lombok.*;

import java.util.Date;

//JDBC - Java Database Connection
//DAO - Data Access Object
//POJO - Plain Old Java Object



//@Getter
//@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
//@RequiredArgsConstructor
public class Book {
//    @Setter(AccessLevel.NONE)
//    @Getter(AccessLevel.NONE)
    private Integer id;
    private String title;
    private String author;
    private Date dateAdded;
}
