package com.sber.spring.springproject.dbexample.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class User {
    private Long id;
    private String login;
    private String password;
    private String fio;
}
