//package com.sber.spring.springproject.dbexample;
//
//import com.sber.spring.springproject.dbexample.dao.impl.BookDaoBeanImpl;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Scope;
//
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//import static com.sber.spring.springproject.dbexample.contants.DataBaseConstants.*;
//
////@ComponentScan
//@Configuration
//public class MyDataBaseContext {
//
//    @Bean
////    @Scope("singleton")
//    public Connection getConnection() throws SQLException {
//        return DriverManager.getConnection("jdbc:postgresql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME,
//                DB_USER,
//                DB_PASSWORD);
//    }
//
////    @Bean
////    public BookDaoBeanImpl getBookDaoBeanImpl() throws SQLException {
////        return new BookDaoBeanImpl(getConnection());
////    }
//}
