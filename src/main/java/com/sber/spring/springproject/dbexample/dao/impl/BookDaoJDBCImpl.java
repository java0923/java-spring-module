//package com.sber.spring.springproject.dbexample.dao.impl;
//
//import com.sber.spring.springproject.dbexample.dao.IDaoLayer;
//import com.sber.spring.springproject.dbexample.database.DatabaseConnection;
//import com.sber.spring.springproject.dbexample.model.Book;
//import lombok.extern.log4j.Log4j;
//import lombok.extern.slf4j.Slf4j;
//
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.List;
//
////CRUD операции
////Create Read Update Delete
//@Slf4j
//public class BookDaoJDBCImpl implements IDaoLayer<Book, Integer> {
//    private static final Connection DB_CONNECTION;
//
//    static {
//        try {
//            DB_CONNECTION = DatabaseConnection.INSTANCE.getConnection();
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public Book getOneObjectById(Integer id) {
//        final String sqlQuery = "select * from books where id = ?";
//        try {
//            PreparedStatement query = DB_CONNECTION.prepareStatement(sqlQuery);
//            query.setInt(1, id);
//            ResultSet result = query.executeQuery();
//
//            Book book = new Book();
//            while (result.next()) {
//                book.setId(result.getInt("id"));
//                book.setAuthor(result.getString("author"));
//                book.setTitle(result.getString("title"));
//                book.setDateAdded(result.getDate("date_added"));
//            }
//            return book;
//
//        } catch (SQLException e) {
//            log.error("Error: " + e.getMessage());
//        }
//        return null;
//    }
//
//
//    @Override
//    public List<Book> getAllObjects() {
//        try (Connection connection = DatabaseConnection.INSTANCE.getConnection()) {
//            final String sqlQuery = "select * from books";
//            PreparedStatement query = connection.prepareStatement(sqlQuery);
//            ResultSet result = query.executeQuery();
//            List<Book> books = new ArrayList<>();
//            while (result.next()) {
//                Book book = new Book();
//                book.setId(result.getInt("id"));
//                book.setAuthor(result.getString("author"));
//                book.setTitle(result.getString("title"));
//                book.setDateAdded(result.getDate("date_added"));
//                books.add(book);
//            }
//            return books;
//
//        } catch (SQLException e) {
//            log.error("Error: " + e.getMessage());
//        }
//        return Collections.emptyList();
//    }
//
//    @Override
//    public Book create(Book newObject) {
//        return null;
//    }
//
//    @Override
//    public Book update(Book updatedObject, Integer integer) {
//        return null;
//    }
//
//    @Override
//    public void deleteObjectById(Integer integer) {
//
//    }
//}
