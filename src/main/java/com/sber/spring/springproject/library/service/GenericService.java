package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.GenericDto;
import com.sber.spring.springproject.library.exception.MyDeleteException;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.GenericMapperImpl;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.repository.GenericRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Абстрактный сервис который хранит в себе реализацию CRUD операций по умолчанию
 * Если реализация отличная от того что представлено в этом классе,
 * то она переопределяется в сервисе для конкретной сущности
 *
 * @param <T> - Сущность с которой мы работаем
 * @param <N> - DTO, которую мы будем отдавать/принимать дальше
 */

@Service
@Slf4j
public abstract class GenericService<T extends GenericModel, N extends GenericDto> {
    protected final GenericRepository<T> repository;
    protected final GenericMapperImpl<T, N> mapper;

    public GenericService(GenericRepository<T> repository,
                          GenericMapperImpl<T, N> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public List<N> listAll() {
        return mapper.toDtos(repository.findAll());
    }

    public List<N> listAllNotDeleted() {
        return mapper.toDtos(repository.findAllByIsDeletedFalse());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> preResult = repository.findAll(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public Page<N> listAllNotDeleted(Pageable pageable) {
        Page<T> preResult = repository.findAllByIsDeletedFalse(pageable);
        List<N> result = mapper.toDtos(preResult.getContent());
        return new PageImpl<>(result, pageable, preResult.getTotalElements());
    }

    public N getOne(final Long id) throws NotFoundException {
        return mapper.toDto(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных не найдено по заданному id")));
    }

    public N create(N newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now());
        newEntity.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        T entity = mapper.toEntity(newEntity);
        return mapper.toDto(repository.save(entity));
    }

    public N update(N updatedEntity, final Long id) {
        updatedEntity.setUpdatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        updatedEntity.setUpdatedWhen(LocalDateTime.now());
        updatedEntity.setCreatedBy(updatedEntity.getCreatedBy());
        updatedEntity.setCreatedWhen(updatedEntity.getCreatedWhen());
        updatedEntity.setId(id);
        return mapper.toDto(repository.save(mapper.toEntity(updatedEntity)));
    }

    public void delete(final Long id) {
        repository.deleteById(id);
    }

    public void deleteSoft(final Long id) throws MyDeleteException, NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        markAsDeleted(obj);
        repository.save(obj);
    }

    public void restore(final Long id) throws NotFoundException {
        T obj = repository.findById(id).orElseThrow(() -> new NotFoundException("Объект не найден"));
        unMarkAsDeleted(obj);
        repository.save(obj);
    }

    private void markAsDeleted(T obj) {
        obj.setDeleted(true);
        obj.setDeletedWhen(LocalDateTime.now());
        obj.setDeletedBy("ADMIN"); //TODO: исправить, когда подключим секурити
    }

    private void unMarkAsDeleted(T obj) {
        obj.setDeleted(false);
        obj.setDeletedWhen(null);
        obj.setDeletedBy(null);
    }
}
