package com.sber.spring.springproject.library.model;

import lombok.Getter;

@Getter
public enum Genre {
    FANTASY("Фантастика"),
    DRAMA("Драма"),
    NOVEL("Роман"),
    SCIENCE_FICTION("Научная фантастика");

    private final String genreTextDisplay;

    Genre(String text) {
        this.genreTextDisplay = text;
    }

//    public String getGenreTextDisplay() {
//        return this.genreTextDisplay;
//    }
}
