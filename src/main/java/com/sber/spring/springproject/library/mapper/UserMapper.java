package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.UserDto;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.model.User;
import com.sber.spring.springproject.library.repository.UserBookRentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class UserMapper extends GenericMapperImpl<User, UserDto> {

    private final UserBookRentRepository userBookRentRepository;

    protected UserMapper(ModelMapper modelMapper,
                         UserBookRentRepository userBookRentRepository) {
        super(User.class, UserDto.class, modelMapper);
        this.userBookRentRepository = userBookRentRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDto.class)
                .addMappings(m -> m.skip(UserDto::setUserBooksRent)).setPostConverter(toDtoConverter());
        modelMapper.createTypeMap(UserDto.class, User.class)
                .addMappings(m -> m.skip(User::setUserBookRents)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(UserDto source, User destination) {
        if (!Objects.isNull(source.getUserBooksRent())) {
            destination.setUserBookRents(userBookRentRepository.findAllById(source.getUserBooksRent()));
        } else {
            destination.setUserBookRents(Collections.emptyList());
        }
    }

    @Override
    protected void mapSpecificFields(User source, UserDto destination) {
        destination.setUserBooksRent(fillIds(source));
    }

    @Override
    protected List<Long> fillIds(User entity) {
        return Objects.isNull(entity) || Objects.isNull(entity.getUserBookRents())
                ? null
                : entity.getUserBookRents().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
