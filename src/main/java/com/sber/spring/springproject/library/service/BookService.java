package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.*;
import com.sber.spring.springproject.library.dto.create.CreateBookRequest;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.BookMapper;
import com.sber.spring.springproject.library.mapper.BookWithAuthorsMapper;
import com.sber.spring.springproject.library.model.Book;
import com.sber.spring.springproject.library.repository.BookRepository;
import com.sber.spring.springproject.library.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.sber.spring.springproject.library.constants.FileConstants.BOOK_IMAGE_UPLOAD_DIRECTORY;
import static com.sber.spring.springproject.library.constants.FileConstants.NO_IMAGE_FILE;

@Service
public class BookService extends GenericService<Book, BookDto> {

    private final BookWithAuthorsMapper bookWithAuthorsMapper;

    public BookService(BookRepository repository,
                       BookMapper mapper,
                       BookWithAuthorsMapper bookWithAuthorsMapper) {
        super(repository, mapper);
        this.bookWithAuthorsMapper = bookWithAuthorsMapper;
    }

    public BookDto createBookByRequest(final CreateBookRequest createBookRequest) {
        BookDto bookDto = new BookDto();
        bookDto.setTitle(createBookRequest.getTitle());
        bookDto.setGenre(createBookRequest.getGenre());
        bookDto.setDescription(createBookRequest.getDescription());
        bookDto.setPublish(createBookRequest.getPublish());
        bookDto.setAmount(createBookRequest.getAmount());
        bookDto.setPublishYear(createBookRequest.getPublishYear());
        bookDto.setStoragePlace(createBookRequest.getStoragePlace());
        bookDto.setPageAmount(createBookRequest.getPageAmount());
        bookDto.setAuthorIds(createBookRequest.getAuthorIds());
        return super.create(bookDto);
    }

    public BookDto addAuthor(final AddBookDto addBookDto) throws NotFoundException {
        BookDto bookDto = getOne(addBookDto.getBookId());
        bookDto.getAuthorIds().add(addBookDto.getAuthorId());
        update(bookDto, addBookDto.getBookId());
        return bookDto;
    }

    public Page<BookWithAuthorsDto> getAllNotDeletedBooksWithAuthors(PageRequest pageRequest) {
        Page<Book> booksPaginated = repository.findAllByIsDeletedFalse(pageRequest);
        List<BookWithAuthorsDto> result = bookWithAuthorsMapper.toDtos(booksPaginated.getContent());
        return new PageImpl<>(result, pageRequest, booksPaginated.getTotalElements());
    }

    public BookWithAuthorsDto getBookWithAuthorDto(final Long bookId) throws NotFoundException {
        Book book = repository.findById(bookId).orElseThrow(() -> new NotFoundException("Book not found"));
        return bookWithAuthorsMapper.toDto(book);
    }

    public BookDto create(final BookDto newBook,
                          MultipartFile file,
                          MultipartFile image) {
        checkFile(newBook, file, image);
        return super.create(newBook);
    }

    public BookDto update(final BookDto bookDtoUpdated,
                          final Long id,
                          MultipartFile file,
                          MultipartFile image) {
        checkFile(bookDtoUpdated, file, image);
        return super.update(bookDtoUpdated, id);
    }

    private void checkFile(BookDto bookDtoUpdated, MultipartFile file, MultipartFile image) {
        if (file != null && file.getSize() > 0) {
            bookDtoUpdated.setFileDirectory(FileHelper.createFile(file));
        }
        if (image != null && image.getSize() > 0) {
            bookDtoUpdated.setImage(FileHelper.createImage(image, BOOK_IMAGE_UPLOAD_DIRECTORY));
        } else {
            bookDtoUpdated.setImage(NO_IMAGE_FILE);
        }
    }

    public Page<BookWithAuthorsDto> findBooks(final BookSearchDto bookSearchDTO,
                                              final PageRequest pageRequest) {
        String genre = bookSearchDTO.getGenre() != null ? String.valueOf(bookSearchDTO.getGenre().ordinal()) : null;
        Page<Book> booksPaginated = ((BookRepository) repository).searchBooks(genre, bookSearchDTO.getBookTitle(), bookSearchDTO.getAuthorFio(), pageRequest);
        List<BookWithAuthorsDto> result = bookWithAuthorsMapper.toDtos(booksPaginated.getContent());
        return new PageImpl<>(result, pageRequest, booksPaginated.getTotalElements());
    }
}
