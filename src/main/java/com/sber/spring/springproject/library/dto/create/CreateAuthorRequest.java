package com.sber.spring.springproject.library.dto.create;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;
import java.util.List;

@Data
@ToString
public class CreateAuthorRequest {
    private String authorFio;
    private String description;
    private LocalDate birthDate;
    private List<Long> bookIds;
}
