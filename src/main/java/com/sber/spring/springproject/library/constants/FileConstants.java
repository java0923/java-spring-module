package com.sber.spring.springproject.library.constants;

public interface FileConstants {
    String BOOK_UPLOAD_DIRECTORY = "files/books/";
    String BOOK_IMAGE_UPLOAD_DIRECTORY = "src/main/resources/static/img/books/";
    String AUTHOR_IMAGE_UPLOAD_DIRECTORY = "src/main/resources/static/img/authors/";
    String NO_IMAGE_FILE = "/img/no_image.jpg";
}
