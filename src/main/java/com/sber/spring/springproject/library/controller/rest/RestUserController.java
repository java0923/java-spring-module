package com.sber.spring.springproject.library.controller.rest;

import com.sber.spring.springproject.library.config.jwt.JWTTokenUtil;
import com.sber.spring.springproject.library.dto.LoginDto;
import com.sber.spring.springproject.library.service.UserService;
import com.sber.spring.springproject.library.service.userdetails.CustomUserDetailsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController()
@RequestMapping("/rest/users")
@RequiredArgsConstructor
@Slf4j
public class RestUserController {
    private final CustomUserDetailsService customUserDetailsService;
    private final UserService userService;
    private final JWTTokenUtil tokenUtil;

    @PostMapping("/auth")
    public ResponseEntity<?> auth(@RequestBody LoginDto loginDto) {
        Map<String, Object> response = new HashMap<>();
        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDto.getLogin());
        log.info("Found User: " + foundUser);
        if (!userService.checkPassword(loginDto.getPassword(), foundUser)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации. Неверный пароль.");
        }
        final String token = tokenUtil.generateToken(foundUser);
        response.put("token", token);
        response.put("username", foundUser.getUsername());
        response.put("roles", foundUser.getAuthorities());
        return ResponseEntity.ok().body(response);
    }
}
