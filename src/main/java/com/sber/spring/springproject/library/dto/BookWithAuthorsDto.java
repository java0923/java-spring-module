package com.sber.spring.springproject.library.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BookWithAuthorsDto extends BookDto {
    private Set<AuthorDto> authors;
}
