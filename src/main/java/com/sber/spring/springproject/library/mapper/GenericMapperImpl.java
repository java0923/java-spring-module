package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.GenericDto;
import com.sber.spring.springproject.library.model.GenericModel;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Абстрактный маппер, который реализует основные операции конвертации ИЗ СУЩНОСТИ В ДТО
 * и обратно. С помощью этого класса мы фиксируем основные методы по работе с маппером,
 * а так-же определили абстрактные методы, которые описывают правила формирования различающихся полей
 *
 * @param <E> - Сущность с которой мы работаем
 * @param <D> - DTO, которую мы будем отдавать/принимать дальше
 */
@Component
@Slf4j
public abstract class GenericMapperImpl<E extends GenericModel, D extends GenericDto>
        implements Mapper<E, D> {
    private final Class<E> entityClass;
    private final Class<D> dtoClass;

    protected final ModelMapper modelMapper;

    public GenericMapperImpl(Class<E> entityClass, Class<D> dtoClass, ModelMapper modelMapper) {
        this.entityClass = entityClass;
        this.dtoClass = dtoClass;
        this.modelMapper = modelMapper;
    }

    @Override
    public E toEntity(D dto) {
        return Objects.isNull(dto)
                ? null
                : modelMapper.map(dto, entityClass);
    }

    @Override
    public D toDto(E entity) {
        return Objects.isNull(entity)
                ? null
                : modelMapper.map(entity, dtoClass);
    }

    @Override
    public List<E> toEntities(List<D> dtoList) {
        return dtoList.stream().map(this::toEntity).toList();
    }

    @Override
    public List<D> toDtos(List<E> entityList) {
        return entityList.stream().map(this::toDto).toList();
    }

    /**
     * Настройка маппера (что делать и что вызывать в случае несовпадения типов данных сорса/дестинейшена)
     */
    @PostConstruct
    protected abstract void setupMapper();

    protected Converter<E, D> toDtoConverter() {
        return context -> {
            E source = context.getSource();
            D destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected Converter<D, E> toEntityConverter() {
        return context -> {
            D source = context.getSource();
            E destination = context.getDestination();
            mapSpecificFields(source, destination);
            return context.getDestination();
        };
    }

    protected abstract void mapSpecificFields(E source, D destination);

    protected abstract void mapSpecificFields(D source, E destination);

    protected abstract List<Long> fillIds(E source);

}
