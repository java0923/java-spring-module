package com.sber.spring.springproject.library.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Where;

import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "authors", schema = "public")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
//@Where(clause = "is_deleted = false")
public class Author extends GenericModel {

    @Column(name = "author_fio", nullable = false)
    private String authorFio;
    @Column(name = "birth_date", nullable = false)
    private LocalDate birthDate;
    @Column(name = "description")
    private String description;
    @Column(name = "image", columnDefinition = "varchar(255) default '/img/no_image.jpg'")
    private String image;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "author_id"), foreignKey = @ForeignKey(name = "FK_AUTHORS_BOOKS"),
            inverseJoinColumns = @JoinColumn(name = "book_id"), inverseForeignKey = @ForeignKey(name = "FK_BOOKS_AUTHORS")
    )
    private List<Book> books;

    @Override
    public String toString() {
        return "Author{" +
                "authorFio='" + authorFio + '\'' +
                ", birthDate=" + birthDate +
                ", description='" + description + '\'' +
                ", books=" + books +
                ", id=" + getId() +
                '}';
    }
}
