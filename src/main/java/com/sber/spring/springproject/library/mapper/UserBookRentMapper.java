package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.UserBookRentDto;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.model.UserBookRent;
import com.sber.spring.springproject.library.repository.BookRepository;
import com.sber.spring.springproject.library.repository.UserRepository;
import com.sber.spring.springproject.library.service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserBookRentMapper extends GenericMapperImpl<UserBookRent, UserBookRentDto> {
    private final BookRepository bookRepository;
    private final UserRepository userRepository;

    private final BookService bookService;

    public UserBookRentMapper(BookRepository bookRepository,
                              UserRepository userRepository,
                              BookService bookService,
                              ModelMapper modelMapper) {
        super(UserBookRent.class, UserBookRentDto.class, modelMapper);
        this.bookRepository = bookRepository;
        this.bookService = bookService;
        this.userRepository = userRepository;
    }

    @Override
    protected void setupMapper() {
        super.modelMapper.createTypeMap(UserBookRent.class, UserBookRentDto.class)
                .addMappings(m -> m.skip(UserBookRentDto::setUserId))
                .addMappings(m -> m.skip(UserBookRentDto::setBookId))
                .addMappings(m -> m.skip(UserBookRentDto::setBookDTO))
                .setPostConverter(toDtoConverter());

        super.modelMapper.createTypeMap(UserBookRentDto.class, UserBookRent.class)
                .addMappings(m -> m.skip(UserBookRent::setUser))
                .addMappings(m -> m.skip(UserBookRent::setBook))
                .setPostConverter(toEntityConverter());

    }

    @Override
    protected void mapSpecificFields(UserBookRent source, UserBookRentDto destination) {
        destination.setUserId(source.getUser().getId());
        destination.setBookId(source.getBook().getId());
        destination.setBookDTO(bookService.getOne(source.getBook().getId()));
    }

    @Override
    protected void mapSpecificFields(UserBookRentDto source, UserBookRent destination) {
        destination.setBook(bookRepository.findById(source.getBookId()).orElseThrow(() -> new NotFoundException("Книги не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected List<Long> fillIds(UserBookRent source) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}
