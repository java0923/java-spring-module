package com.sber.spring.springproject.library.config.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.function.Function;

/*
    kajheg12i736fgqkjhqvei761fiuywvqshaksueg1i76efi2i21yekwh
    header = { "alg": "HS256", "typ": "JWT"}
    payload (JWT-claims)
    payload = { "userId": "b08f86af-35da-48f2-8fab-cef3904660bd", "username":"andy,
                "password":"123", "roles":"ROLE_ADMIN" }
    signature = secret_key + HS256_code_string
     header eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9
     payload eyJ1c2VySWQiOiJiMDhmODZhZi0zNWRhLTQ4ZjItOGZhYi1jZWYzOTA0NjYwYmQifQ
     signature -xN_h82PHVTCMA9vdoHrcZxH-x5mb11y1537t3rGzcM
     =>
     JWT Token
     eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJiMDhmODZhZi0zNWRhLTQ4ZjItOGZhYi1jZWYzOTA0NjYwYmQifQ.-xN_h82PHVTCMA9vdoHrcZxH-x5mb11y1537t3rGzcM
     */
@Component
@Slf4j
public class JWTTokenUtil {
    //7 * 24 * 60 * 1000 = 1 неделя в миллисекундах (время жизни токена)
    private static final long JWT_TOKEN_VALIDITY = 604800000;
    private final String secret = "zdtlD3JK56m6wTTgsNFhqzjqP";

    private static final ObjectMapper OBJECT_MAPPER = getDefaultObjectMapper();

    /*
    Создание токена (генерация)
    Payload => информация о пользователе (CustomUserDetails)
    payload = { "user_id":"1",
                "username":"andy_gavrilov",
                "user_role":"ROLE_ADMIN",
                "password":"13hjbejqgei12y12j3b",
               }
     Указываем время жизни токена и дату создания,
     а также подписываем токен
     */
    public String generateToken(final UserDetails payload) {
        return Jwts.builder()
                .setSubject(payload.toString())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    private static ObjectMapper getDefaultObjectMapper() {
        return new ObjectMapper();
    }

    // проверка на то, что токен не истек
    private Boolean isTokenExpired(final String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    // достаем expirationDate из токена через Claims
    private Date getExpirationDateFromToken(final String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }

    // валидация токена на корректность
    // Пользователь из токена такой же, как из UserDetails и токен не просрочен
    public Boolean validateToken(final String token,
                                 final UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    // достаем username из токена
    public String getUsernameFromToken(final String token) {
        String userNameClaim = getClaimsFromToken(token, Claims::getSubject);
        log.info("userNameClaim: " + userNameClaim);
        JsonNode userNameNode = null;
        try {
            userNameNode = OBJECT_MAPPER.readTree(userNameClaim);
        } catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getUsernameFromToken(): {}", e.getMessage());
        }
        if (userNameNode != null) {
            return userNameNode.get("username").asText();
        } else return null;
    }

    // достаем role из токена
    public String getRoleFromToken(final String token) {
        String roleClaim = getClaimsFromToken(token, Claims::getSubject);
        JsonNode roleNode = null;
        try {
            roleNode = OBJECT_MAPPER.readTree(roleClaim);
        } catch (JsonProcessingException e) {
            log.error("JWTTokenUtil#getRoleFromToken(): {}", e.getMessage());
        }
        if (roleNode != null) {
            return roleNode.get("user_role").asText();
        } else return null;
    }

    private <T> T getClaimsFromToken(final String token,
                                     Function<Claims, T> claimResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }


}
