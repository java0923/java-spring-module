package com.sber.spring.springproject.library.exception;

public class MyDeleteException extends Exception {
    public MyDeleteException(final String message) {
        super(message);
    }
}
