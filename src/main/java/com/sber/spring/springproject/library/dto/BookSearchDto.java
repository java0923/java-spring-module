package com.sber.spring.springproject.library.dto;

import com.sber.spring.springproject.library.model.Genre;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookSearchDto {
    private String bookTitle;
    private String authorFio;
    private Genre genre;
}
