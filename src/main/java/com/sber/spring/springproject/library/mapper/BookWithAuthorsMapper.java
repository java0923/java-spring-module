package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.BookWithAuthorsDto;
import com.sber.spring.springproject.library.model.Book;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.repository.AuthorRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class BookWithAuthorsMapper extends GenericMapperImpl<Book, BookWithAuthorsDto> {

    private AuthorRepository authorRepository;

    public BookWithAuthorsMapper(ModelMapper modelMapper,
                                 AuthorRepository authorRepository) {
        super(Book.class, BookWithAuthorsDto.class, modelMapper);
        this.authorRepository = authorRepository;
    }

    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Book.class, BookWithAuthorsDto.class)
                .addMappings(m -> m.skip(BookWithAuthorsDto::setAuthorIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(BookWithAuthorsDto.class, Book.class)
                .addMappings(m -> m.skip(Book::setAuthors)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Book source, BookWithAuthorsDto destination) {
        destination.setAuthorIds(fillIds(source));
    }

    @Override
    protected void mapSpecificFields(BookWithAuthorsDto source, Book destination) {
        destination.setAuthors(authorRepository.findAllById(source.getAuthorIds()));
    }

    @Override
    protected List<Long> fillIds(Book source) {
        return Objects.isNull(source) || Objects.isNull(source.getId())
                ? null
                : source.getAuthors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
