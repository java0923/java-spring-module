package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.BookDto;
import com.sber.spring.springproject.library.model.Book;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.repository.AuthorRepository;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@Slf4j
public class BookMapper extends GenericMapperImpl<Book, BookDto> {
    private final AuthorRepository authorRepository;

    public BookMapper(ModelMapper modelMapper,
                      AuthorRepository authorRepository) {
        super(Book.class, BookDto.class, modelMapper);
        this.authorRepository = authorRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        // говорим мапперу, чтобы он ничего не делал со списком authorIds у BookDto во время обычного маппинга
        // а вместо этого, использовал Converter, который реализует нашу собственную логику заполнения списка ID авторов
        modelMapper.createTypeMap(Book.class, BookDto.class)
                .addMappings(m -> m.skip(BookDto::setAuthorIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(BookDto.class, Book.class)
                .addMappings(m -> m.skip(Book::setAuthors)).setPostConverter(toEntityConverter());

    }

    /**
     * В данном методе описывается логика заполнения специальных полей, которые есть в DTO, но отсутствуют в Entity
     * В нашем случае, мы достаем у Book ее авторов, и преобразуем List<Author> в List<Long> (храним ID авторов у книги)
     *
     * @param source      наша сущность (entity)
     * @param destination наша DTO
     */
    @Override
    protected void mapSpecificFields(Book source, BookDto destination) {
        destination.setAuthorIds(fillIds(source));
    }

    @Override
    protected void mapSpecificFields(BookDto source, Book destination) {
        if (!Objects.isNull(source.getAuthorIds())) {
            destination.setAuthors(authorRepository.findAllById(source.getAuthorIds()));
        } else {
            destination.setAuthors(Collections.emptyList());
        }
    }

    @Override
    protected List<Long> fillIds(Book source) {
        return Objects.isNull(source) || Objects.isNull(source.getAuthors()) || source.getAuthors().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getAuthors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
