package com.sber.spring.springproject.library.dto;

import com.sber.spring.springproject.library.model.Genre;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDto extends GenericDto {
    private String title;
    private String description;
    private String storagePlace;
    private String publishYear;
    private String publish;
    private Integer pageAmount;
    private Integer amount;
    private Genre genre;
    private List<Long> authorIds = new ArrayList<>();
    private String fileDirectory;
    private String image;
}
