package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.AddBookDto;
import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.dto.BookDto;
import com.sber.spring.springproject.library.dto.create.CreateAuthorRequest;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.AuthorMapper;
import com.sber.spring.springproject.library.model.Author;
import com.sber.spring.springproject.library.repository.AuthorRepository;
import com.sber.spring.springproject.library.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.sber.spring.springproject.library.constants.FileConstants.*;

@Service
public class AuthorService extends GenericService<Author, AuthorDto> {
    public AuthorService(AuthorRepository repository,
                         AuthorMapper mapper) {
        super(repository, mapper);
    }

    public AuthorDto createAuthorByRequest(final CreateAuthorRequest createAuthorRequest) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setAuthorFio(createAuthorRequest.getAuthorFio());
        authorDto.setBirthDate(createAuthorRequest.getBirthDate());
        authorDto.setDescription(createAuthorRequest.getDescription());
        authorDto.setBookIds(createAuthorRequest.getBookIds());
        return super.create(authorDto);
    }

    public AuthorDto create(final AuthorDto newAuthor,
                            MultipartFile image) {
        if (image != null && image.getSize() > 0) {
            newAuthor.setImage(FileHelper.createImage(image, AUTHOR_IMAGE_UPLOAD_DIRECTORY));
        } else {
            newAuthor.setImage(NO_IMAGE_FILE);
        }
        return super.create(newAuthor);
    }

    public AuthorDto update(final AuthorDto authorDtoUpdated,
                            final Long id,
                            MultipartFile image) {
        if (image != null && image.getSize() > 0) {
            authorDtoUpdated.setImage(FileHelper.createImage(image, AUTHOR_IMAGE_UPLOAD_DIRECTORY));
        } else {
            authorDtoUpdated.setImage(NO_IMAGE_FILE);
        }
        return super.update(authorDtoUpdated, id);
    }

    public AuthorDto addBook(final AddBookDto addBookDto) throws NotFoundException {
        AuthorDto authorDto = getOne(addBookDto.getAuthorId());
        authorDto.getBookIds().add(addBookDto.getBookId());
        update(authorDto, addBookDto.getAuthorId());
        return authorDto;
    }

    public Page<AuthorDto> searchAuthors(final String fio,
                                         final Pageable pageable) {
        Page<Author> foundAuthors = ((AuthorRepository) repository).findAllByAuthorFioContainsIgnoreCaseAndIsDeletedFalse(fio, pageable);
        List<AuthorDto> result = mapper.toDtos(foundAuthors.getContent());
        return new PageImpl<>(result, pageable, foundAuthors.getTotalElements());
    }
}
