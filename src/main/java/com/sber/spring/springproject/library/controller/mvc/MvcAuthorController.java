package com.sber.spring.springproject.library.controller.mvc;

import com.sber.spring.springproject.library.dto.AddBookDto;
import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.exception.MyDeleteException;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.service.AuthorService;
import com.sber.spring.springproject.library.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("/authors")
@RequiredArgsConstructor
public class MvcAuthorController {
    private final AuthorService authorService;
    private final BookService bookService;

    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "3") int pageSize,
                          Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "authorFio"));
        Page<AuthorDto> authorDtoList = authorService.listAllNotDeleted(pageRequest);
        model.addAttribute("authors", authorDtoList);
        return "authors/listAllAuthors";
    }

    @GetMapping("/{id}")
    public String getAuthorInfo(@PathVariable Long id,
                                Model model) throws NotFoundException {
        model.addAttribute("author", authorService.getOne(id));
        return "authors/viewAuthor";
    }

    @GetMapping("/add")
    public String create() {
        return "authors/createAuthor";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("authorForm") AuthorDto authorDto,
                         @RequestParam("imageDirectoryInput") MultipartFile image) {
        authorService.create(authorDto, image);
        return "redirect:/authors";
    }

    @GetMapping("/add-book/{authorId}")
    public String addBook(@PathVariable Long authorId,
                          Model model) throws NotFoundException {
        model.addAttribute("books", bookService.listAll());
        model.addAttribute("authorId", authorId);
        model.addAttribute("author", authorService.getOne(authorId).getAuthorFio());
        return "authors/addAuthorBook";
    }

    @PostMapping("/add-book")
    public String addBook(@ModelAttribute("authorBookForm") AddBookDto addBookDTO) throws NotFoundException {
        authorService.addBook(addBookDTO);
        return "redirect:/authors";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        model.addAttribute("author", authorService.getOne(id));
        return "authors/updateAuthor";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("authorForm") AuthorDto authorDto,
                         @RequestParam("fileImageInput") MultipartFile image) {
        authorService.update(authorDto, authorDto.getId(), image);
        return "redirect:/authors";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        authorService.deleteSoft(id);
        return "redirect:/authors";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) throws NotFoundException {
        authorService.restore(id);
        return "redirect:/authors";
    }

    /*
      Поиск авторов
     */
    @PostMapping("/search")
    public String searchAuthors(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "3") int pageSize,
                                @ModelAttribute("authorSearchForm") AuthorDto authorDto,
                                Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "authorFio"));
        model.addAttribute("authors", authorService.searchAuthors(authorDto.getAuthorFio(), pageRequest));
        return "authors/listAllAuthors";
    }
}
