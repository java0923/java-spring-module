package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.RoleDto;
import com.sber.spring.springproject.library.dto.UserDto;
import com.sber.spring.springproject.library.mapper.UserMapper;
import com.sber.spring.springproject.library.model.User;
import com.sber.spring.springproject.library.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class UserService extends GenericService<User, UserDto> {
    @Value("${spring.security.user.name}")
    private String adminUser;
    private final BCryptPasswordEncoder passwordEncoder;

    public UserService(UserRepository repository,
                       UserMapper mapper,
                       BCryptPasswordEncoder passwordEncoder) {
        super(repository, mapper);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto create(UserDto userDto) {
        RoleDto roleDto = new RoleDto();
        String userName = SecurityContextHolder.getContext().getAuthentication().getName();
        if (adminUser.equals(userName)) {
            roleDto.setId(2L); //Роль библиотекаря
        } else {
            roleDto.setId(1L); //Роль пользователя
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        userDto.setRole(roleDto);
        return super.create(userDto);
    }

    public boolean checkPassword(final String passwordToCheck,
                                 final UserDetails userDetails) {
        return passwordEncoder.matches(passwordToCheck, userDetails.getPassword());
    }

    public UserDto getUserByLogin(final String login) {
        return mapper.toDto(((UserRepository) repository).findUserByLoginAndDeletedFalse(login));
    }

    public UserDto getUserByEmail(final String email) {
        return mapper.toDto(((UserRepository) repository).findUserByEmailAndDeletedFalse(email));
    }

    public UserDto getUserByPhone(final String phone) {
        return mapper.toDto(((UserRepository) repository).findUserByPhoneAndDeletedFalse(phone));
    }

    public Page<UserDto> findUsers(UserDto userDTO,
                                   Pageable pageable) {
        Page<User> users = ((UserRepository) repository).searchUsers(userDTO.getFirstName(),
                userDTO.getLastName(),
                userDTO.getLogin(),
                pageable);
        List<UserDto> result = mapper.toDtos(users.getContent());
        return new PageImpl<>(result, pageable, users.getTotalElements());
    }

    public void sendChangePasswordEmail(final UserDto userDTO) {
        UUID uuid = UUID.randomUUID();
        userDTO.setChangePasswordToken(uuid.toString());
        update(userDTO, userDTO.getId());
    }

    public void changePassword(final String uuid,
                               final String password) {
        UserDto userDTO = mapper.toDto(((UserRepository) repository).findUserByChangePasswordToken(uuid));
        userDTO.setChangePasswordToken(null);
        userDTO.setPassword(passwordEncoder.encode(password));
        update(userDTO, userDTO.getId());
    }
}
