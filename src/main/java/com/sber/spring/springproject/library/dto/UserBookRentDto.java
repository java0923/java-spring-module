package com.sber.spring.springproject.library.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserBookRentDto
        extends GenericDto {
    private LocalDate rentDate;
    private LocalDate returnDate;
    private Boolean returned;
    private Integer rentPeriod;
    private Long bookId;
    private Long userId;
    private BookDto bookDTO;
}
