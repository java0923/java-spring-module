package com.sber.spring.springproject.library.controller.mvc;

import com.sber.spring.springproject.library.dto.UserBookRentDto;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.service.BookRentService;
import com.sber.spring.springproject.library.service.BookService;
import com.sber.spring.springproject.library.service.userdetails.CustomUserDetails;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@Slf4j
@Hidden
@RequestMapping("/rent")
@RequiredArgsConstructor
public class MvcRentBookController {

    private final BookRentService rentService;
    private final BookService bookService;


    @GetMapping("/book/{bookId}")
    public String rentBook(@PathVariable Long bookId,
                           Model model) throws NotFoundException {
        model.addAttribute("book", bookService.getOne(bookId));
        return "userBooks/rentBook";
    }

    @PostMapping("/book")
    public String rentBook(@ModelAttribute("rentBookInfo") UserBookRentDto userBookRentDto) throws NotFoundException {
        log.info(userBookRentDto.toString());
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userBookRentDto.setUserId(customUserDetails.getId());
        rentService.rentABook(userBookRentDto);
        return "redirect:/rent/user-books/" + customUserDetails.getId();
    }

    @GetMapping("/return-book/{id}")
    public String returnBook(@PathVariable Long id) {
        CustomUserDetails customUserDetails = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        rentService.returnBook(id);
        return "redirect:/rent/user-books/" + customUserDetails.getId();
    }

    @GetMapping("/user-books/{id}")
    public String userBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                            @RequestParam(value = "size", defaultValue = "5") int pageSize,
                            @PathVariable Long id,
                            Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize);
        Page<UserBookRentDto> rentInfoDTOPage = rentService.listUserRentBooks(id, pageRequest);
        model.addAttribute("rentBooks", rentInfoDTOPage);
        model.addAttribute("userId", id);
        return "userBooks/viewAllUserBooks";
    }
}
