package com.sber.spring.springproject.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "user_book_rents", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class UserBookRent extends GenericModel {
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_USER_BOOK_RENT"))
    private User user;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "book_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_BOOK_USER_RENT"))
    private Book book;

    @Column(nullable = false)
    private LocalDate rentDate;

    // поле автоматически должно рассчитываться из логики: rent_date + rent_period
    @Column(nullable = false)
    private LocalDate returnDate;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean returned;

    // количество дней аренды книги, по умолчанию = 14 дней
    @Column(nullable = false)
    private Integer rentPeriod;

}
