package com.sber.spring.springproject.library.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RoleDto {
    private Long id;
    private String title;
    private String description;
}
