package com.sber.spring.springproject.library.controller.rest;

import com.sber.spring.springproject.library.dto.AddBookDto;
import com.sber.spring.springproject.library.dto.BookDto;
import com.sber.spring.springproject.library.dto.create.CreateBookRequest;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.service.BookService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/books")
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Книги",
        description = "Контроллер для работы с книгами библиотеки")
@SecurityRequirement(name = "Bearer Authentication")
public class BookController {
    private final BookService bookService;

    @Operation(description = "Получить список книг")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BookDto>> getAllBooks() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(bookService.listAll());
    }

    // localhost:9098/books/getBook?id=1
    @RequestMapping(value = "/getBook", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDto> getOneBook(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(bookService.getOne(id));
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDto> create(@RequestBody CreateBookRequest newBook) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(bookService.createBookByRequest(newBook));
    }

    @RequestMapping(value = "/updateBook", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDto> update(@RequestBody BookDto updatedBook,
                                          @RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(bookService.update(updatedBook, id));
    }

    // @RequestParam: localhost:9098/books/deleteBook?id=1
    // @PathVariable: localhost:9098/books/deleteBook/1
    @RequestMapping(value = "/deleteBook/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        bookService.delete(id);
    }

    /**
     * Добавление автора к книге
     * @param addBookDto AddDTO Request
     * @return BookDTO
     * @throws NotFoundException BookNotFound Exception
     */
    @Operation(description = "Добавить автора к книге")
    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BookDto> addAuthor(@RequestBody AddBookDto addBookDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(bookService.addAuthor(addBookDto));
    }
}






