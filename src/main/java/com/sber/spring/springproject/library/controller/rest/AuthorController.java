package com.sber.spring.springproject.library.controller.rest;

import com.sber.spring.springproject.library.dto.AddBookDto;
import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.dto.create.CreateAuthorRequest;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.service.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/authors")
@Slf4j
@Tag(name = "Авторы",
        description = "Контроллер для авторов библиотеки")
@RequiredArgsConstructor
@SecurityRequirement(name = "Bearer Authentication")
public class AuthorController {
    private final AuthorService authorService;

    @Operation(description = "Получить список авторов")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AuthorDto>> getAllAuthors() {
        return ResponseEntity.status(HttpStatus.OK)
                //.header("MyHeader", "This is value for my header")
                .body(authorService.listAll());
    }

    // localhost:9098/books/getBook?id=1
    @RequestMapping(value = "/getAuthor", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDto> getOneAuthor(@RequestParam(value = "id") Long id) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(authorService.getOne(id));
    }

    @RequestMapping(value = "/addAuthor", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDto> create(@RequestBody CreateAuthorRequest newAuthor) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(authorService.createAuthorByRequest(newAuthor));
    }

    @RequestMapping(value = "/updateAuthor", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDto> update(@RequestBody AuthorDto updatedBook,
                                            @RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(authorService.update(updatedBook, id));
    }

    // @RequestParam: localhost:9098/books/deleteBook?id=1
    // @PathVariable: localhost:9098/books/deleteBook/1
    @RequestMapping(value = "/deleteAuthor/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        authorService.delete(id);
    }

    @RequestMapping(value = "/addBook", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AuthorDto> addBook(@RequestBody AddBookDto addBookDto) throws NotFoundException {
        return ResponseEntity.status(HttpStatus.CREATED).body(authorService.addBook(addBookDto));
    }

}
