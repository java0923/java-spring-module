package com.sber.spring.springproject.library.dto.create;

import com.sber.spring.springproject.library.dto.RoleDto;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDate;

@Data
@ToString
public class CreateUserRequest {
    private String login;
    private String password;
    private String email;
    private LocalDate birthDate;
    private String firstName;
    private String lastName;
    private String middleName;
    private String phone;
    private String address;
    private RoleDto role;
}
