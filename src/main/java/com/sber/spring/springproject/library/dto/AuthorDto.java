package com.sber.spring.springproject.library.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDto extends GenericDto {
    private String authorFio;
    private LocalDate birthDate;
    private String description;
    private String image;
    private List<Long> bookIds;

    @Override
    public String toString() {
        return "AuthorDto{" +
                "authorFio='" + authorFio + '\'' +
                ", birthDate=" + birthDate +
                ", description='" + description + '\'' +
                ", bookIds=" + bookIds +
                ", id=" + id +
                '}';
    }
}
