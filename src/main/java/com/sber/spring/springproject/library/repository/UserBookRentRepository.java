package com.sber.spring.springproject.library.repository;

import com.sber.spring.springproject.library.model.UserBookRent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public interface UserBookRentRepository extends GenericRepository<UserBookRent> {
    Page<UserBookRent> getBookRentInfoByUserId(Long id,
                                               Pageable pageRequest);
}
