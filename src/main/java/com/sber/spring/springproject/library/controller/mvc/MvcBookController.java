package com.sber.spring.springproject.library.controller.mvc;

import com.sber.spring.springproject.library.dto.*;
import com.sber.spring.springproject.library.dto.BookDto;
import com.sber.spring.springproject.library.exception.MyDeleteException;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping("/books")
@Slf4j
public class MvcBookController {

    private final BookService bookService;

    public MvcBookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping("")
    public String listAll(@RequestParam(value = "page", defaultValue = "1") int page,
                          @RequestParam(value = "size", defaultValue = "3") int pageSize,
                          Model model) {
        // Aspect log input
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        Page<BookWithAuthorsDto> bookDtoList = bookService.getAllNotDeletedBooksWithAuthors(pageRequest);
        model.addAttribute("books", bookDtoList);
        // Aspect log exit
        return "books/listAllBooks";
    }

    @GetMapping("/{id}")
    public String getAuthorInfo(@PathVariable Long id,
                                Model model) throws NotFoundException {
        model.addAttribute("book", bookService.getBookWithAuthorDto(id));
        return "books/viewBook";
    }

    @GetMapping("/add")
    public String create() {
        return "books/createBook";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("bookForm") BookDto bookDto,
                         @RequestParam("fileDirectoryInput") MultipartFile file,
                         @RequestParam("imageDirectoryInput") MultipartFile image) {
        bookService.create(bookDto, file, image);
        return "redirect:/books";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         Model model) throws NotFoundException {
        model.addAttribute("book", bookService.getOne(id));
        return "books/updateBook";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute("bookForm") BookDto bookDto,
                         @RequestParam("fileDirectoryInput") MultipartFile file,
                         @RequestParam("fileImageInput") MultipartFile image
    ) {
        bookService.update(bookDto, bookDto.getId(), file, image);
        return "redirect:/books";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) throws MyDeleteException, NotFoundException {
        bookService.deleteSoft(id);
        return "redirect:/books";
    }

    @GetMapping("/restore/{id}")
    public String restore(@PathVariable Long id) throws NotFoundException {
        bookService.restore(id);
        return "redirect:/books";
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Resource> downloadBook(@RequestParam(value = "bookId") Long id) throws NotFoundException, IOException {
        BookDto bookDto = bookService.getOne(id);
        Path path = Paths.get(bookDto.getFileDirectory());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    private HttpHeaders headers(final String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store");
        headers.add("Expires", "0");
        return headers;
    }

    @PostMapping("/search")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "3") int pageSize,
                              @ModelAttribute("bookSearchForm") BookSearchDto bookSearchDTO,
                              Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, pageSize, Sort.by(Sort.Direction.ASC, "title"));
        model.addAttribute("books", bookService.findBooks(bookSearchDTO, pageRequest));
        return "books/listAllBooks";
    }

    /**
     * Метод для поиска книги по ФИО автора (редирект по кнопке "Посмотреть книги" на странице автора)
     *
     * @param page      - текущая страница
     * @param pageSize  - количество объектов на странице
     * @param authorDTO - ДТО автора
     * @param model     - модель
     * @return - форму со списком всех книг подходящих под критерии (по фио автора)
     */
    @PostMapping("/search/author")
    public String searchBooks(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "3") int pageSize,
                              @ModelAttribute("authorSearchForm") AuthorDto authorDTO,
                              Model model) {
        BookSearchDto bookSearchDTO = new BookSearchDto();
        bookSearchDTO.setAuthorFio(authorDTO.getAuthorFio());
        return searchBooks(page, pageSize, bookSearchDTO, model);
    }

}
