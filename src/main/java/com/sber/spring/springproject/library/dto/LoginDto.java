package com.sber.spring.springproject.library.dto;

import lombok.Data;

@Data
public class LoginDto {
    private String login;
    private String password;
}
