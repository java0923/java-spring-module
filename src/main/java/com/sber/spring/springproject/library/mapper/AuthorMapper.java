package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.model.Author;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.repository.BookRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AuthorMapper extends GenericMapperImpl<Author, AuthorDto> {
    private final BookRepository bookRepository;

    public AuthorMapper(ModelMapper modelMapper,
                        BookRepository bookRepository) {
        super(Author.class, AuthorDto.class, modelMapper);
        this.bookRepository = bookRepository;
    }

    @PostConstruct
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(Author.class, AuthorDto.class)
                .addMappings(m -> m.skip(AuthorDto::setBookIds)).setPostConverter(toDtoConverter());

        modelMapper.createTypeMap(AuthorDto.class, Author.class)
                .addMappings(m -> m.skip(Author::setBooks)).setPostConverter(toEntityConverter());
    }

    @Override
    protected void mapSpecificFields(Author source, AuthorDto destination) {
        destination.setBookIds(fillIds(source));
    }

    @Override
    protected void mapSpecificFields(AuthorDto source, Author destination) {
        if (!Objects.isNull(source.getBookIds())) {
            destination.setBooks(bookRepository.findAllById(source.getBookIds()));
        } else {
            destination.setBooks(Collections.emptyList());
        }
    }

    @Override
    protected List<Long> fillIds(Author source) {
        return Objects.isNull(source) || Objects.isNull(source.getBooks()) || source.getBooks().isEmpty()
                ? Collections.EMPTY_LIST
                : source.getBooks().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toList());
    }
}
