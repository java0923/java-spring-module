package com.sber.spring.springproject.library.constants;

import java.util.List;

public interface SecurityRequestsConstants {
    List<String> RESOURCES_WHITE_LIST = List.of("/resources/**",
            "/static/**",
            "/js/**",
            "/css/**",
            "/",
            "/fonts/**",
            "/img/**",
            "/swagger-ui/**",
            "/webjars/bootstrap/5.0.2/**",
            "/v3/api-docs/**");

    List<String> BOOKS_WHITE_LIST = List.of("/books",
            "/books/search",
            "/books/{id}");

    List<String> BOOKS_REST_WHITE_LIST = List.of("/rest/books/list",
            "/rest/books/getBook/**");

    List<String> AUTHORS_WHITE_LIST = List.of("/authors",
            "/authors/search",
            "/books/search/author",
            "/authors/{id}");
    List<String> BOOKS_PERMISSION_LIST = List.of("/books/add",
            "/rest/books/addBook",
            "/rest/books/updateBook",
            "/rest/books/deleteBook/{id}",
            "/books/addAuthor",
            "/books/update/*",
            "/books/delete/*",
            "/books/download/{bookId}");

    List<String> AUTHORS_PERMISSION_LIST = List.of("/authors/add",
            "/authors/addAuthor",
            "/authors/updateAuthor",
            "/authors/addBook",
            "/authors/deleteAuthor/{id}",
            "/authors/update/*",
            "/authors/delete/*");


    List<String> USERS_WHITE_LIST = List.of("/login",
            "/users/registration",
            "/users/remember-password",
            "/users/change-password",
            "/users/auth");

    List<String> USERS_PERMISSION_LIST = List.of("/rent/book/**");
    List<String> ADMINS_PERMISSION_LIST = List.of("/users/list/**");
}
