package com.sber.spring.springproject.library.mapper;

import com.sber.spring.springproject.library.dto.GenericDto;
import com.sber.spring.springproject.library.model.GenericModel;

import java.util.List;

/*
Интерфейс, имеющий основной набор методов для преобразования сущности в DTO и наоборот
 */
public interface Mapper<E extends GenericModel, D extends GenericDto> {
    E toEntity(D dto);

    D toDto(E entity);

    List<E> toEntities(List<D> dtoList);

    List<D> toDtos(List<E> entityList);
}
