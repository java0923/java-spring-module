package com.sber.spring.springproject.library.repository;

import com.sber.spring.springproject.library.model.Author;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuthorRepository extends GenericRepository<Author> {

    Page<Author> findAllByAuthorFioContainsIgnoreCaseAndIsDeletedFalse(String fio, Pageable pageable);
}
