package com.sber.spring.springproject.library.dto.create;

import com.sber.spring.springproject.library.model.Genre;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class CreateBookRequest {
    private String title;
    private String description;
    private String storagePlace;
    private String publishYear;
    private String publish;
    private Integer pageAmount;
    private Integer amount;
    private Genre genre;
    private List<Long> authorIds;
}
