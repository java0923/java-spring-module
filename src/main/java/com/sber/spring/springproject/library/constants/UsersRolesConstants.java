package com.sber.spring.springproject.library.constants;

public interface UsersRolesConstants {
    String USER = "USER";
    String LIBRARIAN = "LIBRARIAN";
}
