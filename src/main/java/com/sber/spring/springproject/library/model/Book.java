package com.sber.spring.springproject.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "books", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Book extends GenericModel {
    @Column(name = "title", nullable = false)
    private String title;
    @Column(nullable = false)
    private String description;
    private String storagePlace;
    @Column(name = "publish_year", nullable = false)
    private String publishYear;
    @Column(name = "publish", nullable = false)
    private String publish;
    @Column(name = "page_amount", nullable = false)
    private Integer pageAmount;
    @Column(name = "amount", nullable = false)
    private Integer amount;
    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    private Genre genre;
//    @Transient
//    private String libraryName;

    @Column(name = "file_directory")
    private String fileDirectory;
    @Column(name = "image", columnDefinition = "varchar(255) default '/img/no_image.jpg'")
    private String image;

    private LocalDateTime createdWhen;
    private String createdBy;
    private LocalDateTime updatedWhen;
    private String updatedBy;

//    @JsonIgnore
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "books_authors",
            joinColumns = @JoinColumn(name = "book_id"), foreignKey = @ForeignKey(name = "FK_BOOKS_AUTHORS"),
            inverseJoinColumns = @JoinColumn(name = "author_id"), inverseForeignKey = @ForeignKey(name = "FK_AUTHORS_BOOKS")
    )
    private List<Author> authors;

//    @JsonIgnore
    @OneToMany(mappedBy = "book", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<UserBookRent> userBookRents;

    /* Про каскады:
     *
     * @OneToMany(cascade = CascadeType.PERSIST)
     * Book -> many Reviews (книга имеет много отзывов от читателей) -> отношение 1-М
     * Book book = new Book("NAME");
     * Review review1 = new Review("Good");
     * Review review2 = new Review("Excellent");
     * book.addReview(r1);
     * book.addReview(r2);
     * bookRepository.save(book);
     * =>
     * как результат выполнения данного кода, будет выполнено 3 запроса:
     * insert into books()....;
     * insert into reviews(r1)....;
     * insert into reviews(r2)....;
     * -----------------------------------------------------------------------------------
     * @OneToMany(cascade = CascadeType.MERGE)
     * Book book = bookRepository.findById(1L);
     * book.setDescription("New Description");
     *
     * Review r1 = reviewRepository.findById(4L);
     * r1.setScore(3);
     * bookRepository.save(book);
     * =>
     * update books set description = ? and id = book.id;
     * update reviews set mark = ? where id = review.id;
     * -----------------------------------------------------------------------------------
     * @OneToMany(cascade = CascadeType.REMOVE)
     * Book book = bookRepository.findById(1L);
     * bookRepository.delete(book);
     * =>
     * delete from reviews where id = ?;
     * delete from reviews where id = ?;
     * delete from books where id = ?;
     * --orphanRemoval = true
     * =>
     * @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true)
     * Book book = bookRepository.findById(1L);
     * book.removeReview(book.getReviews().get(0));
     * =>
     * delete from reviews where id = ?;
     */
}
