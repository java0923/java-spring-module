package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.BookDto;
import com.sber.spring.springproject.library.dto.UserBookRentDto;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.UserBookRentMapper;
import com.sber.spring.springproject.library.model.UserBookRent;
import com.sber.spring.springproject.library.repository.UserBookRentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class BookRentService extends GenericService<UserBookRent, UserBookRentDto> {
    private final BookService bookService;
    private final UserBookRentRepository userBookRentRepository;

    public BookRentService(UserBookRentRepository repository,
                           UserBookRentMapper mapper,
                           BookService bookService,
                           UserBookRentRepository userBookRentRepository) {
        super(repository, mapper);
        this.bookService = bookService;
        this.userBookRentRepository = userBookRentRepository;
    }

    public void rentABook(final UserBookRentDto userBookRentDto) throws NotFoundException {
        BookDto bookDto = bookService.getOne(userBookRentDto.getBookId());
        bookDto.setAmount(bookDto.getAmount() - 1);
        bookService.update(bookDto, bookDto.getId());
        int rentPeriod = userBookRentDto.getRentPeriod() != null ? userBookRentDto.getRentPeriod() : 14;
        userBookRentDto.setRentDate(LocalDate.now());
        userBookRentDto.setReturned(false);
        userBookRentDto.setRentPeriod(rentPeriod);
        userBookRentDto.setCreatedWhen(LocalDateTime.now());
        userBookRentDto.setReturnDate(LocalDate.now().plusDays(rentPeriod));
        userBookRentDto.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        mapper.toDto(userBookRentRepository.save(mapper.toEntity(userBookRentDto)));
    }

    public Page<UserBookRentDto> listUserRentBooks(final Long id,
                                                   final Pageable pageRequest) {
        Page<UserBookRent> objects = userBookRentRepository.getBookRentInfoByUserId(id, pageRequest);
        List<UserBookRentDto> results = mapper.toDtos(objects.getContent());
        return new PageImpl<>(results, pageRequest, objects.getTotalElements());
    }

    public void returnBook(final Long id) {
        UserBookRentDto bookRentInfoDTO = getOne(id);
        bookRentInfoDTO.setReturned(true);
        bookRentInfoDTO.setReturnDate(LocalDate.now());
        BookDto bookDTO = bookRentInfoDTO.getBookDTO();
        bookDTO.setAmount(bookDTO.getAmount() + 1);
        update(bookRentInfoDTO, id);
        bookService.update(bookDTO, bookDTO.getId());
    }
}
