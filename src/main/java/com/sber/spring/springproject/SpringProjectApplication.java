package com.sber.spring.springproject;

import com.sber.spring.springproject.library.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@SpringBootApplication
@Slf4j
//@RequiredArgsConstructor
public class SpringProjectApplication implements CommandLineRunner {
    //    @Autowired
//    private BookDaoBeanImpl bookDaoBean;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    //Вариант инъекции через конструктор
//    public SpringProjectApplication(BookDaoBeanImpl bookDaoBean) {
//        this.bookDaoBean = bookDaoBean;
//    }

    //Вариант инъекции через сеттер
//    @Autowired
//    public void setBookDaoBean(BookDaoBeanImpl bookDaoBean) {
//        this.bookDaoBean = bookDaoBean;
//    }


    public static void main(String[] args) {
        SpringApplication.run(SpringProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Swagger runs on: /swagger-ui/index.html");
//        BookDaoJDBCImpl bookDaoJDBC = new BookDaoJDBCImpl();
//        Book book = bookDaoJDBC.getOneObjectById(1);
//        log.info(book.toString());
//        вариант расширения контекста своими настройками
//        ApplicationContext ctx = new AnnotationConfigApplicationContext(MyDataBaseContext.class);
//        BookDaoBeanImpl bookDaoBean = ctx.getBean(BookDaoBeanImpl.class);
//        log.info(bookDaoBean.getOneObjectById(1).toString());
//        log.info(bookDaoBean.getAllObjects().toString());

//        log.info(bookDaoBean.getAllObjects().toString());
//        List<Book> books = jdbcTemplate.query("select * from books",
//                (rs, rowNum) -> new Book(
//                        rs.getInt("id"),
//                        rs.getString("author"),
//                        rs.getString("title"),
//                        rs.getDate("date_added")
//                ));
//        books.forEach(System.out::println);
//    }
    }
}
