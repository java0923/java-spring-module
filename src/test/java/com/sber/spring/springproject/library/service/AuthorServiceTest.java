package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.AddBookDto;
import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.AuthorMapper;
import com.sber.spring.springproject.library.model.Author;
import com.sber.spring.springproject.library.repository.AuthorRepository;
import com.sber.spring.springproject.library.service.data.AuthorTestData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class AuthorServiceTest extends GenericTest<Author, AuthorDto> {

    public AuthorServiceTest() {
        repository = Mockito.mock(AuthorRepository.class);
        mapper = Mockito.mock(AuthorMapper.class);
        service = new AuthorService((AuthorRepository) repository, (AuthorMapper) mapper);
    }

    @Order(3)
    @Test
    @Override
    protected void getAll() {
        Mockito.when(repository.findAll())
                .thenReturn(AuthorTestData.AUTHOR_LIST);
        Mockito.when(mapper.toDtos(AuthorTestData.AUTHOR_LIST))
                .thenReturn(AuthorTestData.AUTHOR_DTO_LIST);
        List<AuthorDto> authorDtos = service.listAll();
        log.info("Testing getAll(): " + authorDtos.toString());
        assertEquals(AuthorTestData.AUTHOR_LIST.size(), authorDtos.size());
    }

    @Order(2)
    @Test
    @Override
    protected void getOne() throws NotFoundException {
        Author author1 = AuthorTestData.AUTHOR_1;
        author1.setId(1L);

        AuthorDto authorDtoInitial = AuthorTestData.AUTHOR_DTO_1;
        authorDtoInitial.setId(1L);

        Author author2 = AuthorTestData.AUTHOR_2;
        author2.setId(2L);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(author1));
        Mockito.when(repository.findById(2L)).thenReturn(Optional.of(author2));
        Mockito.when(mapper.toDto(author1)).thenReturn(authorDtoInitial);
        Mockito.when(mapper.toDto(author2)).thenReturn(AuthorTestData.AUTHOR_DTO_2);

        AuthorDto authorDto1 = service.getOne(1L);
        AuthorDto authorDto2 = service.getOne(2L);
        log.info("Testing getOne(): " + authorDto1);
        log.info("Testing getOne(): " + authorDto2);
        assertEquals(authorDto1, AuthorTestData.AUTHOR_DTO_1);
        assertNotEquals(authorDto2, AuthorTestData.AUTHOR_DTO_1);
    }

    @Order(1)
    @Test
    @Override
    protected void create() {
        Mockito.when(mapper.toEntity(AuthorTestData.AUTHOR_DTO_1)).thenReturn(AuthorTestData.AUTHOR_1);
        Mockito.when(mapper.toDto(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_1);
        AuthorDto authorDto = service.create(AuthorTestData.AUTHOR_DTO_1);
        log.info("Testing create(): " + authorDto);
        assertEquals(authorDto, AuthorTestData.AUTHOR_DTO_1);
    }

    @Order(4)
    @Test
    @Override
    protected void update() {
        Mockito.when(mapper.toEntity(AuthorTestData.AUTHOR_DTO_1)).thenReturn(AuthorTestData.AUTHOR_1);
        Mockito.when(mapper.toDto(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_1);
        AuthorDto authorDto = service.update(AuthorTestData.AUTHOR_DTO_1, 1L);
        log.info("Testing update(): " + authorDto);
        assertEquals(authorDto, AuthorTestData.AUTHOR_DTO_1);
    }

    @Order(5)
    @Test
    @Override
    protected void delete() {
//        Mockito.d(repository.deleteById(3L)).thenReturn(AuthorTestData.AUTHOR_DTO_3_DELETED);
//        service.delete(3L);
    }

    @Test
    @Order(6)
    void addBook() throws NotFoundException {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(AuthorTestData.AUTHOR_1));
        Mockito.when(service.getOne(1L)).thenReturn(AuthorTestData.AUTHOR_DTO_1);
        Mockito.when(repository.save(AuthorTestData.AUTHOR_1)).thenReturn(AuthorTestData.AUTHOR_1);
        ((AuthorService) service).addBook(new AddBookDto(1L, 1L));
        log.info("Testing addBook(): " + AuthorTestData.AUTHOR_DTO_1.getBookIds());
        assertFalse(AuthorTestData.AUTHOR_DTO_1.getBookIds().isEmpty());
    }
}
