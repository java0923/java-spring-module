package com.sber.spring.springproject.library.service;

import com.sber.spring.springproject.library.dto.GenericDto;
import com.sber.spring.springproject.library.exception.NotFoundException;
import com.sber.spring.springproject.library.mapper.GenericMapperImpl;
import com.sber.spring.springproject.library.model.GenericModel;
import com.sber.spring.springproject.library.repository.GenericRepository;
import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public abstract class GenericTest<E extends GenericModel, D extends GenericDto> {
    protected GenericService<E, D> service;
    protected GenericRepository<E> repository;
    protected GenericMapperImpl<E, D> mapper;

    @BeforeEach
    void init() {
        //TODO: добавить логику на авторизацию, как только она будет готова
    }

    protected abstract void getAll();

    protected abstract void getOne() throws NotFoundException;

    protected abstract void create();

    protected abstract void update();

    protected abstract void delete();
}
