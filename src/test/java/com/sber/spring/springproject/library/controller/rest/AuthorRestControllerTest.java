package com.sber.spring.springproject.library.controller.rest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.sber.spring.springproject.library.controller.GenericControllerTest;
import com.sber.spring.springproject.library.dto.AuthorDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class AuthorRestControllerTest extends GenericControllerTest {
    @Override
    protected void createObject() throws Exception {

    }

    @Override
    protected void updateObject() throws Exception {

    }

    @Override
    protected void deleteObject() throws Exception {

    }

    @Test
    @Order(1)
    @Override
    protected void listAll() throws Exception {
        log.info("Testing listAll authors via REST started");
        String result = mvc.perform(
                        MockMvcRequestBuilders.get("/authors/list")
//                        .headers()
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
                .andReturn()
                .getResponse()
                .getContentAsString();
        List<AuthorDto> authorDtos = objectMapper.readValue(result, new TypeReference<List<AuthorDto>>() {});
        assertFalse(authorDtos.isEmpty());
        authorDtos.forEach(a -> log.info("Author: " + a.toString()));
        log.info("Testing listAll authors via REST finished");
    }

    @Override
    protected void listOne() throws Exception {

    }
}
