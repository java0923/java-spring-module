package com.sber.spring.springproject.library.service.data;

import com.sber.spring.springproject.library.dto.AuthorDto;
import com.sber.spring.springproject.library.model.Author;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface AuthorTestData {
    AuthorDto AUTHOR_DTO_1 = new AuthorDto("authorFio1",
            LocalDate.now(),
            "description1",
            "",
            new ArrayList<>());

    AuthorDto AUTHOR_DTO_2 = new AuthorDto("authorFio2",
            LocalDate.now(),
            "description2",
            "",
            new ArrayList<>());

    AuthorDto AUTHOR_DTO_3_DELETED = new AuthorDto("authorFio3",
            LocalDate.now(),
            "description3",
            "",
            new ArrayList<>());

    List<AuthorDto> AUTHOR_DTO_LIST = Arrays.asList(AUTHOR_DTO_1, AUTHOR_DTO_2, AUTHOR_DTO_3_DELETED);


    Author AUTHOR_1 = new Author("author1",
            LocalDate.now(),
            "description1",
            "",
            null);

    Author AUTHOR_2 = new Author("author2",
            LocalDate.now(),
            "description2",
            "",
            null);

    Author AUTHOR_3 = new Author("author3",
            LocalDate.now(),
            "description3",
            "",
            null);

    List<Author> AUTHOR_LIST = Arrays.asList(AUTHOR_1, AUTHOR_2, AUTHOR_3);
}
